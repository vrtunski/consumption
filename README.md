## Synopsis

Energy consumption usecase application that will process metering data coming from customers.
## Build and Run

Gradle wrapper is provided which can be used to build application by running command - gradlew.bat build - or just - gradle build - command
Application can be started - gradlew bootRun or - gradle bootRun

## API Reference

For API reference docs spring-rest docs project is used. By running gradlew asciidoctor command it will generate HTML documentation which can be found on build\asciidoc\html5\api-guide.html location

## Tests

Test can be run by command - gradle :cleanTest :test
Also, application can be tested via postman or any other tool that can be used to construct HTTP REST requests.
