/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.repository;

import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.enums.Month;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author branislav.vrtunski
 */
@Repository
public interface ProfileRepository extends CrudRepository<Profile, Long>, JpaSpecificationExecutor {

     Profile findByMonthAndName(Month month, String name);
}
