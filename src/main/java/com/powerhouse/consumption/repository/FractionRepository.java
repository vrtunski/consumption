/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.repository;

import com.powerhouse.consumption.entity.Fraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author branislav.vrtunski
 */
@Repository
public interface FractionRepository extends JpaRepository<Fraction, Integer>{
    
}
