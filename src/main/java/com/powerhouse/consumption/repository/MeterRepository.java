/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.repository;

import com.powerhouse.consumption.entity.Meter;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author branislav.vrtunski
 */
@Repository
public interface MeterRepository extends CrudRepository<Meter, Long>, JpaSpecificationExecutor {

    Meter findByConnectionIdAndProfile_Id(String connectionId, int profileId);
}
