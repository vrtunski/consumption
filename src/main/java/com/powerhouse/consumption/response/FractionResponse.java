/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.response;

import com.powerhouse.consumption.enums.Month;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author branislav.vrtunski
 */
@Data
@NoArgsConstructor
public class FractionResponse {
    @Enumerated(EnumType.STRING)
    private Month month;
    private String profile;
    private double fraction;
}
