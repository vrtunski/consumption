/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.service;

import com.powerhouse.consumption.entity.Meter;
import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.repository.MeterRepository;
import com.powerhouse.consumption.request.MeterRequest;
import com.powerhouse.consumption.request.MetersRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

/**
 *
 * @author branislav.vrtunski
 */
@Service
@Transactional
public class MeterService {

    @Inject
    private MeterRepository meterRepository;

    @Inject
    private MeterValidator meterValidator;

    @Inject
    private ProfileService profileService;

    /**
     * Method for validation and storing importing meters
     * @param metersRequest
     * @param errors 
     */
    public void saveMeters(MetersRequest metersRequest, final BindingResult errors) {

        Map<String, List<MeterRequest>> sortedMetersMap = new HashMap();
        metersRequest.meters.stream().map(MeterRequest::getConnectionId).distinct().forEach(connectionId -> sortedMetersMap.put(connectionId,
                metersRequest.meters.stream().filter(object -> connectionId.equals(object.getConnectionId())).collect(Collectors.toList())));

        sortedMetersMap.keySet().forEach((connectionId) -> {
            sortedMetersMap.get(connectionId).sort((MeterRequest o1, MeterRequest o2) -> o1.getMonth().compareTo(o2.getMonth()));
        });

        sortedMetersMap.entrySet().forEach(entry -> {
            meterValidator.validate(entry.getValue(), errors);
            entry.getValue().stream().forEach(meter -> {
                if (errors.hasErrors()) {
                    System.out.println("Meter saving error" + errors.getAllErrors().stream().map(Object::toString).
                            collect(Collectors.joining(",")));

                } else {
                    final Profile profile = profileService.findProfile(meter.getMonth(), meter.getProfile());
                    Meter m = new Meter();
                    m.setConnectionId(meter.getConnectionId());
                    m.setReading(meter.getReading());
                    m.setProfile(profile);
                    meterRepository.save(m);
                }
            });
        });

    }

    /**
     * Method for finding consumption for given month
     * @param connectionId
     * @param month
     * @param profile
     * @return consumption 
     */
    public Long findMeterConsumption(final String connectionId, final Month month, final String profile) {
        Profile currentProfile = profileService.findProfile(month, profile);
        Meter currentMeter = meterRepository.findByConnectionIdAndProfile_Id(connectionId, currentProfile.getId());
        if (month == Month.JAN) {
            return currentMeter.getReading();
        }
        Month previousMonth = Month.values()[month.ordinal() - 1];
        Profile previousProfile = profileService.findProfile(previousMonth, profile);
        Meter prevoiusMeter = meterRepository.findByConnectionIdAndProfile_Id(connectionId, previousProfile.getId());

        return currentMeter.getReading() - prevoiusMeter.getReading();
    }

}
