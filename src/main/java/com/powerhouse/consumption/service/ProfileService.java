/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.service;

import com.powerhouse.consumption.entity.Fraction;
import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.exceptions.BusinessProcessException;
import com.powerhouse.consumption.repository.ProfileRepository;
import com.powerhouse.consumption.request.FractionRequest;
import com.powerhouse.consumption.request.FractionsRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author branislav.vrtunski
 */
@Service
@Transactional
public class ProfileService {

    @Inject
    private ProfileRepository profileRepository;

    /**
     * Method for validation and saving all imported fractions
     *
     * @param fractionsRequest
     */
    public void saveFractions(FractionsRequest fractionsRequest) {
        if (!validateFractions(fractionsRequest)) {
            throw new BusinessProcessException(1000, "Sum of the fractions is not equal to 1.0");
        }

        fractionsRequest.getFractionRequests().stream().map((fraction) -> {
            Fraction ratio = new Fraction();
            ratio.setRatio(fraction.getFraction());
            Profile profile = new Profile();
            profile.setMonth(fraction.getMonth());
            profile.setName(fraction.getProfile());
            profile.setFraction(ratio);
            ratio.setProfile(profile);
            return profile;
        }).forEachOrdered((profile) -> {
            profileRepository.save(profile);
        });
    }

    public Profile findProfile(Month month, String name) {
        return profileRepository.findByMonthAndName(month, name);
    }

    /**
     * Validation method for fractions: sum of all fractions ratio must be 1
     *
     * @param fractionsRequest
     * @return boolean
     */
    private boolean validateFractions(FractionsRequest fractionsRequest) {
        Map<String, List<FractionRequest>> sortedProfileMap = new HashMap();
        fractionsRequest.getFractionRequests().stream().map(FractionRequest::getProfile).distinct().forEach(profile -> sortedProfileMap.put(profile,
                fractionsRequest.getFractionRequests().stream().filter(object -> profile.equals(object.getProfile())).collect(Collectors.toList())));

        double maxSum = 1d;
        return sortedProfileMap.keySet().stream().map((key) -> {
            List<FractionRequest> profileFractionList = sortedProfileMap.get(key);
            double sum = profileFractionList.stream().mapToDouble(FractionRequest::getFraction).sum();
            return sum;
        }).noneMatch((sum) -> (maxSum != sum));

    }
}
