/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.service;

import com.powerhouse.consumption.entity.Fraction;
import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.request.MeterRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import javax.inject.Inject;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author branislav.vrtunski
 */
@Component
public class MeterValidator implements Validator {

    private static final double TOLERANCE_HIGHER = 1.25;

    private static final double TOLERANCE_LOWER = 0.75;

    @Inject
    private ProfileService profileService;


    @Override
    public boolean supports(Class<?> clazz) {
        return MeterRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final List<MeterRequest> meters = (List<MeterRequest>) target;

        validateMeterReading(meters, errors);
        final Profile profile = profileService.findProfile(meters.get(0).getMonth(), meters.get(0).getProfile());
       if (profile != null) {
            validateConsumption(profile, meters, errors);
        } else {
            errors.reject("1002", "Profile Not Found");
        }
    }

    private void validateMeterReading(final List<MeterRequest> meters, final Errors errors) {
        Map<MeterRequest, MeterRequest> errorReadingsMap = meters.stream().collect(MeterErrors.collector());
        if (errorReadingsMap.size() > 0) {
            errors.reject("1001", "Meter reading is wrong");
        }
    }

    private static final class MeterErrors {

        private Map<MeterRequest, MeterRequest> map = new HashMap<>();

        private MeterRequest meterCurrent;
        private MeterRequest meterNext;

        public void accept(MeterRequest meterReading) {
            meterCurrent = meterNext;
            meterNext = meterReading;
            if (meterCurrent != null && meterNext != null && meterCurrent.getReading().compareTo(meterNext.getReading()) > 0) {
                map.put(meterCurrent, meterNext);
            }
        }

        public MeterErrors combine(MeterErrors other) {
            throw new UnsupportedOperationException("Parallel Stream not supported");
        }

        public Map<MeterRequest, MeterRequest> finish() {
            return map;
        }

        public static Collector<MeterRequest, ?, Map<MeterRequest, MeterRequest>> collector() {
            return Collector.of(MeterErrors::new, MeterErrors::accept, MeterErrors::combine, MeterErrors::finish);
        }

    }

    private void validateConsumption(final Profile profile, final List<MeterRequest> meters,
            final Errors errors) {

        final MeterRequest lastMeter = meters.get(meters.size() - 1);
        final BigDecimal totalConsumption = new BigDecimal(lastMeter.getReading());

        final Map<MeterRequest, BigDecimal> meterReadingConsumptions = meters.stream().collect(ConsumptionError.collector());
        meterReadingConsumptions.forEach((meter, meterConsumption) -> {
            final Fraction fraction = profile.getFraction();
            if (fraction == null) {
                errors.reject("1003", "fraction doesn't exist");
                return;
            }
            if (!checkTolerance(fraction, meterConsumption, totalConsumption)) {
                errors.reject("1004", "Meter consumption for meter ID:" + meter.getConnectionId() + " in month " + meter.getMonth() + " is wrong");
            }
        });
    }

    private boolean checkTolerance(final Fraction fraction, final BigDecimal meterConsumption, final BigDecimal totalConsumption) {
        BigDecimal expectedConsumption = totalConsumption.multiply(new BigDecimal(fraction.getRatio()));
        return isBetween(meterConsumption, expectedConsumption.multiply(BigDecimal.valueOf(TOLERANCE_LOWER)),
                expectedConsumption.multiply(BigDecimal.valueOf(TOLERANCE_HIGHER)));

    }

    private boolean isBetween(BigDecimal actual, BigDecimal low, BigDecimal high) {
        return actual.subtract(low).signum() >= 0 && actual.subtract(high).signum() <= 0;
    }
    
    private static final class ConsumptionError {

	    private Map<MeterRequest, BigDecimal> map = new HashMap<>();

	    private MeterRequest meterReadingCurrent, meterReadingNext;

	    public void accept(MeterRequest meterReading) {
	    	meterReadingCurrent = meterReadingNext;
	    	meterReadingNext = meterReading;
	    	BigDecimal actualConsumption = BigDecimal.ZERO;
			if (meterReadingNext != null && meterReadingNext.getMonth() == Month.JAN){
	    		 actualConsumption = new BigDecimal(meterReadingNext.getReading());
	    	} else if(meterReadingCurrent != null && meterReadingCurrent.getMonth() == Month.DEC){
	    		actualConsumption = new BigDecimal(meterReadingCurrent.getReading());
	    	} else if (meterReadingNext != null && meterReadingCurrent != null) {
	    			actualConsumption = new BigDecimal(meterReadingNext.getReading()).subtract(new BigDecimal(meterReadingCurrent.getReading()));
	    	}
	    	map.put(meterReadingNext, actualConsumption);
	    }

	    public ConsumptionError combine(ConsumptionError other) {
	        throw new UnsupportedOperationException("Parallel Stream not supported");
	    }

	    public Map<MeterRequest, BigDecimal> finish() {
	        return map;
	    }

	    public static Collector<MeterRequest, ?, Map<MeterRequest, BigDecimal>> collector() {
	        return Collector.of(ConsumptionError::new, ConsumptionError::accept, ConsumptionError::combine, ConsumptionError::finish);
	    }

	}

}
