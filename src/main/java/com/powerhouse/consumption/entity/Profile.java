/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.entity;

/**
 *
 * @author branislav.vrtunski
 */
import com.powerhouse.consumption.enums.Month;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "PROFILE",uniqueConstraints={@UniqueConstraint(columnNames={"month", "name"})})
@Data
@ToString(callSuper = true, exclude = {"fraction", "meter"})
@EqualsAndHashCode(exclude = {"fraction", "meter"})
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private int id;
    
    @NotNull
    @Column(name = "month")
    @Enumerated(EnumType.STRING)
    private Month month;
    
    @NotNull
    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL)
    private Fraction fraction;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL)
    private Meter meter;

}
