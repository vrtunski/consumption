/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author branislav.vrtunski
 */
@Entity
@Table(name = "METER")
@Data
@ToString(callSuper = true, exclude = {"profile"})
@EqualsAndHashCode(exclude = {"profile"})
public class Meter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "connectionId")
    private String connectionId;

    @NotNull
    @Column(name = "reading")
    private Long reading;

    @OneToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Profile profile;

}
