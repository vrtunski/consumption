/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.request;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author branislav.vrtunski
 */
@Data
@NoArgsConstructor
public class MetersRequest {
    
    public List<MeterRequest> meters;
    
}
