/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.enums;

/**
 *
 * @author branislav.vrtunski
 */
public enum Month {
    JAN, FEB, MAR, APR, MAY, JUN, JUL, AVG, SEP, OCT, NOV, DEC;
}
