package com.powerhouse.consumption.exceptions;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author branislav.vrtunski
 * 
 */
@ControllerAdvice
@Slf4j
public class BusinessProcessExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Invalid request handler.
     *
     * @param e the {@link RuntimeException}
     * @param request the {@link WebRequest}
     * @return a {@link ResponseEntity}
     */
    @ExceptionHandler({ BusinessProcessException.class })
    protected ResponseEntity<Object> handleInvalidRequest(RuntimeException e, WebRequest request) {
        BusinessProcessException businessProcessException = (BusinessProcessException)e;
        log.error(businessProcessException.getMessage(), businessProcessException);

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(businessProcessException.getErrorCode());
        errorResponse.setMessage(businessProcessException.getMessage());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return handleExceptionInternal(e, errorResponse, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
    }
}
