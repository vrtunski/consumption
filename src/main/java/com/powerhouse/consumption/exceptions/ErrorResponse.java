package com.powerhouse.consumption.exceptions;

/**
 * @author branislav.vrtunski
 * <br>
 * 
 */
public class ErrorResponse {
    private int errorCode = 0;
    private String message;

    public ErrorResponse() {
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
