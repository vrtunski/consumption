package com.powerhouse.consumption.exceptions;

import java.text.MessageFormat;

/**
 * @author branislav.vrtunski
 * <br>
 *
 */
public class BusinessProcessException extends RuntimeException{

    private int errorCode = 0;
    private String message;
    private String messagePattern;
    private Object[] messagePatternArgs;


    public BusinessProcessException(int errorCode, String message) {
        super(message);
        this.message = message;
        this.errorCode = errorCode;
    }

    public BusinessProcessException(int errorCode, String message, Exception e) {
        super(message, e);
        this.message = message;
        this.errorCode = errorCode;
    }

    public BusinessProcessException(int errorCode, String messagePattern, Object ... args) {
        super();
        this.errorCode = errorCode;
        this.messagePattern = messagePattern;
        this.messagePatternArgs = args;
        try {
            MessageFormat messageFormat = new MessageFormat(messagePattern);
            this.message = messageFormat.format(args);
        } catch (Exception e) {
            throw new RuntimeException("Cannot create BusinessProcessException.", e);
        }
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BusinessProcessException{");
        sb.append("errorCode=").append(errorCode);
        sb.append(", message='").append(message).append('\'');
        sb.append(", messagePattern='").append(messagePattern).append('\'');
        sb.append(", messagePatternArgs=").append(messagePatternArgs);
        sb.append('}');
        return sb.toString();
    }
}
