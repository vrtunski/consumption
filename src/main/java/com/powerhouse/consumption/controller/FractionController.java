/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.controller;

import com.powerhouse.consumption.entity.Fraction;
import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.repository.FractionRepository;
import com.powerhouse.consumption.request.FractionRequest;
import com.powerhouse.consumption.request.FractionsRequest;
import com.powerhouse.consumption.service.ProfileService;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author branislav.vrtunski
 */
@RestController
@RequestMapping(FractionController.REST_BASE_URL)
public class FractionController {
    
    public final static String REST_BASE_URL = "fraction-service";

    @Inject
    private ProfileService profileService;
    
    @Inject
    private FractionRepository fractionRepository;

    /**
     * Rest endpoint for importing all fractions
     * @param fractionsRequest 
     */
    @RequestMapping(value = "/fractions", method = POST, headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus( HttpStatus.CREATED )
    public void importFractions(@RequestBody FractionsRequest fractionsRequest) {
        profileService.saveFractions(fractionsRequest);
    }
    /**
     * Rest endpoint for retrieving all fractions.
     * @return list of fraction objects
     */
    @RequestMapping(value = "/fractions", method = GET, headers = "Accept=application/json", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<Fraction>> getAllFractions() {
        return new ResponseEntity<>(fractionRepository.findAll(), HttpStatus.OK);
    }
    /**
     * Rest endpoint for retrieving a fraction by id.
     * @param fractionId
     * @return fraction
     */
    @RequestMapping(value = "/fractions/{fractionId}", method = GET, headers = "Accept=application/json", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Object> getFraction(@PathVariable("fractionId") int fractionId) {
        return new ResponseEntity<>(fractionRepository.findOne(fractionId), HttpStatus.OK);
    }
    /**
     * 
     * @param fractionId 
     */
    @RequestMapping(value = "/fractions/{fractionId}", method = DELETE, headers = "Accept=application/json", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus( HttpStatus.OK )
    public void deleteFraction(@PathVariable("fractionId") int fractionId) {
        fractionRepository.delete(fractionId);
    }
    /**
     * Rest endpoint for updating a fraction by id.
     * @param fractionId
     * @param fractionRequest 
     */
    @RequestMapping(value = "/fractions/{fractionId}", method = PUT, headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus( HttpStatus.CREATED )
    public void updateFraction(@PathVariable("fractionId") int fractionId, @RequestBody FractionRequest fractionRequest) {
        Fraction fraction  = new Fraction();
        Profile profile = profileService.findProfile(fractionRequest.getMonth(), fractionRequest.getProfile());      
        fraction.setId(fractionId);
        fraction.setProfile(profile);
        fraction.setRatio(fractionRequest.getFraction());
        fractionRepository.save(fraction);
    }

}
