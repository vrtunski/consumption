/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.controller;

import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.service.MeterService;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author branislav.vrtunski
 */
@RestController
@RequestMapping(ConsumptionController.REST_BASE_URL)
public class ConsumptionController {

    public final static String REST_BASE_URL = "consumption-service";

    @Inject
    private MeterService meterService;

    /**
     * Rest endpoint for retrieving consumption of given meter for a given month
     * @param connectionId
     * @param month
     * @param profile
     * @return consumption for month
     */
    @RequestMapping(value = "/consumption/{connectionId}/{month}/{profile}", method = GET, headers = "Accept=application/json", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> getConsumptionForMonth(@PathVariable final String connectionId, @PathVariable final Month month, @PathVariable final String profile) {
        final Long consumption = meterService.findMeterConsumption(connectionId, month, profile);
        return new ResponseEntity<>(consumption, HttpStatus.OK);
    }
}
