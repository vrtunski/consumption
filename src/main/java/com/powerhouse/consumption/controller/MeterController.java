/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.controller;

import com.powerhouse.consumption.entity.Meter;
import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.repository.MeterRepository;
import com.powerhouse.consumption.request.MeterRequest;
import com.powerhouse.consumption.request.MetersRequest;
import com.powerhouse.consumption.service.MeterService;
import com.powerhouse.consumption.service.ProfileService;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author branislav.vrtunski
 */
@RestController
@RequestMapping(MeterController.REST_BASE_URL)
public class MeterController {

    public final static String REST_BASE_URL = "meter-service";

    @Inject
    private MeterService meterService;
    
    @Inject
    private MeterRepository meterRepository;
    
    @Inject
    private ProfileService profileService;

    /**
     * Rest endpoint for storing all importing meters
     * @param metersRequest
     * @param errors 
     */
    @RequestMapping(value = "/meters", method = POST, headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void importMeters(@Valid @RequestBody MetersRequest metersRequest, final BindingResult errors) {
        meterService.saveMeters(metersRequest, errors);
    }
    /**
     * Rest endpoint for retrieving all meters.
     * @return list of meter objects
     */
    @RequestMapping(value = "/meters", method = GET, headers = "Accept=application/json", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<Meter>> getAllMeters() {
        return new ResponseEntity<>((List<Meter>) meterRepository.findAll(), HttpStatus.OK);
    }
    /**
     * Rest endpoint for retrieving a meterReading by id.
     * @param meterId
     * @return meter
     */
    @RequestMapping(value = "/meters/{meterId}", method = GET, headers = "Accept=application/json", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Meter> getMeter(@PathVariable("meterId") Long meterId) {
        return new ResponseEntity<>(meterRepository.findOne(meterId), HttpStatus.OK);
    }
    /**
     * Rest endpoint for deleting a meter by  id.
     * @param meterId 
     */
    @RequestMapping(value = "/meters/{meterId}", method = DELETE, headers = "Accept=application/json", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus( HttpStatus.OK )
    public void deleteMeter(@PathVariable("meterId") Long meterId) {
        meterRepository.delete(meterId);
    }
    /**
     * Rest endpoint for updating a meter reading by meter id.
     * @param meterId
     * @param meterRequest 
     */
    @RequestMapping(value = "/meters/{meterId}", method = PUT, headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus( HttpStatus.CREATED )
    public void updateMeter(@PathVariable("meterId") Long meterId, @RequestBody MeterRequest meterRequest) {
        Meter meter  = new Meter();
        Profile profile = profileService.findProfile(meterRequest.getMonth(), meterRequest.getProfile());      
        meter.setId(meterId);
        meter.setProfile(profile);
        meter.setConnectionId(meterRequest.getConnectionId());
        meter.setReading(meterRequest.getReading());
        meterRepository.save(meter);
    }
}
