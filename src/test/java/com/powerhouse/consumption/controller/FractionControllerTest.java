/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.controller;

import com.powerhouse.consumption.TestBase;
import static com.powerhouse.consumption.controller.FractionController.REST_BASE_URL;
import com.powerhouse.consumption.entity.Fraction;
import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.repository.FractionRepository;
import com.powerhouse.consumption.request.FractionRequest;
import com.powerhouse.consumption.request.FractionsRequest;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import jersey.repackaged.com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import org.springframework.restdocs.payload.JsonFieldType;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author branislav.vrtunski
 */
@RunWith(SpringRunner.class)
@AutoConfigureRestDocs(outputDir = "build/generated-snippets")
@WebMvcTest(controllers = {FractionController.class}, secure = false)
public class FractionControllerTest extends TestBase {

    private static final String URL_BASE = "/" + REST_BASE_URL + "/";

    @Inject
    private MockMvc mockMvc;

    @Inject
    private FractionRepository fractionRepositoryMock;

    @Test
    public void test_getAllFractions() throws Exception {

        final Fraction fraction = new Fraction();
        fraction.setRatio(0.1);
        final List<Fraction> fractionsPage = Lists.newArrayList(fraction);
        when(fractionRepositoryMock.findAll()).thenReturn(fractionsPage);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(URL_BASE + "fractions");
        this.mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), responseFields(
                        fieldWithPath("[].id").type(JsonFieldType.NUMBER)
                                .description("The fraction id"),
                        fieldWithPath("[].ratio").type(JsonFieldType.NUMBER)
                                .description("The fraction value"))));

        verify(fractionRepositoryMock).findAll();
    }

    @Test
    public void test_getOneFraction() throws Exception {
        final Long fractionId = Long.valueOf(1);
        final Fraction fraction = new Fraction();
        fraction.setRatio(0.5);
        when(fractionRepositoryMock.findOne(fractionId.intValue())).thenReturn(fraction);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(URL_BASE + "fractions/{fractionId}", fractionId);
        this.mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andDo(document("{method-name}",
                        responseFields(
                                fieldWithPath("id").type(JsonFieldType.NUMBER)
                                        .description("The fraction id"),
                                fieldWithPath("ratio").type(JsonFieldType.NUMBER)
                                        .description("The fraction value"))));
        verify(fractionRepositoryMock, atLeastOnce()).findOne(anyInt());
    }

    @Test
    public void test_importFraction() throws Exception {
        final FractionsRequest fractionsRequest = new FractionsRequest();
        final FractionRequest fractionRequest = new FractionRequest();
        fractionRequest.setProfile("A");
        fractionRequest.setMonth(Month.OCT);
        fractionRequest.setFraction(0.5);
        fractionsRequest.setFractionRequests(new ArrayList<>());
        fractionsRequest.getFractionRequests().add(fractionRequest);

        mockMvc.perform(post(URL_BASE + "fractions").contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(fractionsRequest))).andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated());

    }

    @Test
    public void test_deleteFraction() throws Exception {
        final int fractionId = 1;

        mockMvc.perform(delete(URL_BASE + "fractions/{fractionId}", fractionId).contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isOk())
                .andDo(document("{method-name}"
                ));
        verify(fractionRepositoryMock).delete(any(Integer.class));
    }

}
