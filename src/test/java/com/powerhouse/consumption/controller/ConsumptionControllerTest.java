/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.controller;

import com.powerhouse.consumption.TestBase;
import static com.powerhouse.consumption.controller.ConsumptionController.REST_BASE_URL;
import com.powerhouse.consumption.entity.Meter;
import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.repository.MeterRepository;
import com.powerhouse.consumption.service.ProfileService;
import javax.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author branislav.vrtunski
 */
@RunWith(SpringRunner.class)
@AutoConfigureRestDocs(outputDir = "build/generated-snippets")
@WebMvcTest(controllers = {MeterController.class}, secure = false)
public class ConsumptionControllerTest extends TestBase {

    private static final String URL_BASE = "/" + REST_BASE_URL + "/";
    @Inject
    private MockMvc mockMvc;

    @Inject
    private MeterRepository meterRepositoryMock;

    @Inject
    private ProfileService profileServiceMock;

    @Test
    public void test_getConsumptionForMonth() throws Exception {
        final String connectionId = "0001";
        final Month month = Month.JAN;
        final String profile = "A";
        final Profile cProfile = new Profile();
        cProfile.setId(1);
        cProfile.setMonth(Month.JAN);
        cProfile.setName("A");
        final Meter meter = new Meter();
        meter.setConnectionId("0001");
        meter.setProfile(cProfile);
        meter.setReading(120L);
        when(profileServiceMock.findProfile(month, profile)).thenReturn(cProfile);
        when(meterRepositoryMock.findByConnectionIdAndProfile_Id(connectionId, cProfile.getId())).thenReturn(meter);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(URL_BASE + "consumption/{connectionId}/{month}/{profile}", connectionId, month, profile);
        this.mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andDo(document("{method-name}"));
       
    }

}
