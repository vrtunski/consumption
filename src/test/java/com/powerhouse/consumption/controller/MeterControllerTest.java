/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.controller;

import com.powerhouse.consumption.TestBase;
import static com.powerhouse.consumption.controller.MeterController.REST_BASE_URL;
import com.powerhouse.consumption.entity.Meter;
import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.repository.MeterRepository;
import com.powerhouse.consumption.request.MeterRequest;
import com.powerhouse.consumption.request.MetersRequest;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import jersey.repackaged.com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import org.springframework.restdocs.payload.JsonFieldType;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author branislav.vrtunski
 */
@RunWith(SpringRunner.class)
@AutoConfigureRestDocs(outputDir = "build/generated-snippets")
@WebMvcTest(controllers = {MeterController.class}, secure = false)
public class MeterControllerTest extends TestBase {

    private static final String URL_BASE = "/" + REST_BASE_URL + "/";
    @Inject
    private MockMvc mockMvc;

    @Inject
    private MeterRepository meterRepositoryMock;
    
     @Test
    public void test_getAllMeters() throws Exception {

        final Meter meter = new Meter();
        meter.setConnectionId("0001");
        meter.setReading(150L);
        final List<Meter> metersPage = Lists.newArrayList(meter);
        when(meterRepositoryMock.findAll()).thenReturn(metersPage);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(URL_BASE + "meters");
        this.mockMvc.perform(builder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), responseFields(
                        fieldWithPath("[].id").type(JsonFieldType.NULL)
                                .description("The meter id"),
                        fieldWithPath("[].connectionId").type(JsonFieldType.STRING)
                                .description("The connection id"),
                        fieldWithPath("[].reading").type(JsonFieldType.NUMBER)
                                .description("The meter reading"))));

        verify(meterRepositoryMock).findAll();
    }

    @Test
    public void test_getOneMeter() throws Exception {
        final Long meterId = Long.valueOf(1);
        final Meter meter = new Meter();
        meter.setReading(120L);
        meter.setConnectionId("0001");
        when(meterRepositoryMock.findOne(meterId)).thenReturn(meter);
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(URL_BASE + "meters/{meterId}", meterId);
        this.mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andDo(document("{method-name}",
                        responseFields(
                                fieldWithPath("id").type(JsonFieldType.NULL)
                                .description("The meter id"),
                                fieldWithPath("connectionId").type(JsonFieldType.STRING)
                                        .description("The meter connectionId"),
                                fieldWithPath("reading").type(JsonFieldType.NUMBER)
                                        .description("The meter reading"))));
        verify(meterRepositoryMock, atLeastOnce()).findOne(anyLong());
    }

    @Test
    public void test_importMeters() throws Exception {
        final MetersRequest metersRequest = new MetersRequest();
        final MeterRequest meterRequest = new MeterRequest();
        meterRequest.setProfile("A");
        meterRequest.setMonth(Month.OCT);
        meterRequest.setReading(120L);
        metersRequest.setMeters(new ArrayList<>());
        metersRequest.getMeters().add(meterRequest);

        mockMvc.perform(post(URL_BASE + "meters").contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(metersRequest))).andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated());

    }

    @Test
    public void test_deleteMeter() throws Exception {
        final Long meterId = 1L;

        mockMvc.perform(delete(URL_BASE + "meters/{meterId}", meterId).contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isOk())
                .andDo(document("{method-name}"
                ));
        verify(meterRepositoryMock).delete(any(Long.class));
    }


}
