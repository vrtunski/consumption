/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption.service;

import com.powerhouse.consumption.entity.Fraction;
import com.powerhouse.consumption.entity.Profile;
import com.powerhouse.consumption.enums.Month;
import com.powerhouse.consumption.repository.ProfileRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author branislav.vrtunski
 */
public class ProfileServiceTest {

    @Mock
    private ProfileRepository profileRepository;

    @InjectMocks
    private ProfileService profileService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        profileService = new ProfileService();
    }

    @Test
    public void test_saveFractions() throws Exception {
        when(profileRepository.save(any(Profile.class))).thenReturn(createProfile());
        profileRepository.save(any(Profile.class));
        verify(profileRepository).save(any(Profile.class));
    }

    private Profile createProfile() {
        Fraction ratio = new Fraction();
        ratio.setRatio(0.5);
        Profile profile = new Profile();
        profile.setMonth(Month.APR);
        profile.setName("A");
        profile.setFraction(ratio);
        ratio.setProfile(profile);
        return profile;
    }

}
