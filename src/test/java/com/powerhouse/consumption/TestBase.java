/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.powerhouse.consumption;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerhouse.consumption.controller.ConsumptionController;
import com.powerhouse.consumption.controller.FractionController;
import com.powerhouse.consumption.controller.MeterController;
import com.powerhouse.consumption.repository.FractionRepository;
import com.powerhouse.consumption.repository.MeterRepository;
import com.powerhouse.consumption.repository.ProfileRepository;
import com.powerhouse.consumption.service.MeterService;
import com.powerhouse.consumption.service.MeterValidator;
import com.powerhouse.consumption.service.ProfileService;
import java.io.IOException;
import static org.mockito.Mockito.mock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.SpringDataWebConfiguration;

/**
 *
 * @author branislav.vrtunski
 */
public abstract class TestBase {

    protected byte[] convertObjectToJsonBytes(final Object object) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Configuration
    public static class TestConfig extends SpringDataWebConfiguration {

        @Bean
        public ProfileService profileService() {
            return mock(ProfileService.class);
        }

        @Bean
        public ProfileRepository profileRepository() {
            return mock(ProfileRepository.class);
        }

        @Bean
        public FractionRepository fractionRepository() {
            return mock(FractionRepository.class);
        }

        @Bean
        public MeterRepository meterRepository() {
            return mock(MeterRepository.class);
        }

        @Bean
        public FractionController fractionController() {
            return new FractionController();
        }
        
        @Bean
        public MeterService meterService() {
            return mock(MeterService.class);
        }
        @Bean
        public MeterValidator meterValidator() {
            return mock(MeterValidator.class);
        }

        @Bean
        public MeterController meterController() {
            return new MeterController();
        }
        @Bean
        public ConsumptionController consumptionController() {
            return new ConsumptionController();
        }

    }

}
